﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SampleItems
{
	public static List<ItemData> samples = new List<ItemData>() {
		new ItemData("Bomba", "Bomba que explota al cabo de un rato", "Smashbrositems_1", 1),
		new ItemData("Concha Verde", "Una concha que se puede lanzar en una dirección", "Smashbrositems_6", 1),
		new ItemData("Martillo", "Un martillo para dar mazazos", "Smashbrositems_8", 1),
		new ItemData("Abanico", "Un abanico para la calor", "Smashbrositems_4", 1),
		new ItemData("Corazón", "Un corazón que te permite recuperar 1 vida", "Smashbrositems_9", 1)
	};

}
