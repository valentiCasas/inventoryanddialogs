﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryControllerScript : MonoBehaviour {

	public GameObject m_itemPrefab;

	private int m_itemsPerPage = 6;
	private bool m_isShown = true;

	private CanvasGroup m_cg;
	private RectTransform m_rectTrans;
	private List<GameObject> m_items;

	void Start () 
	{
		m_cg = GetComponent<CanvasGroup> ();
		m_rectTrans = gameObject.GetComponent<RectTransform> ();
		m_items = new List<GameObject> ();

		addRandomItem ();
		addRandomItem ();
		addRandomItem ();
	}

	public void addRandomItem()
	{
		float casilla = (m_rectTrans.rect.width / m_itemsPerPage);

		float width = (casilla / 2) + m_items.Count * casilla;
		float height = m_rectTrans.rect.height / 2;

		// Comprobar si el objeto existe para añadir 1 más
		int indice = Random.Range (0, SampleItems.samples.Count - 1);
		ItemData item = SampleItems.samples [indice];

		int posicio = alreadyInInventory (item);

		if (posicio != -1) 
		{
			m_items [posicio].GetComponent<IventoryItemScript> ().addItem ();
		} 
		else 
		{
			GameObject obj = Instantiate (m_itemPrefab, new Vector3(width, height, 0.0f), Quaternion.Euler (Vector3.zero));
			obj.GetComponent<IventoryItemScript> ().loadItem (item.m_name, item.m_image, item.m_description, 1);
			obj.transform.SetParent (gameObject.transform, true);
			m_items.Add(obj);
		}
	}

	private int alreadyInInventory(ItemData item)
	{
		int already = -1;

		for (int i = 0; i < m_items.Count; i++) 
		{
			ItemData data = m_items[i].GetComponent<IventoryItemScript> ().getItemData();

			if (data.m_name == item.m_name) 
			{
				already = i;
				break;
			}
		}

		return already;
	}

	public void ShowHideInventory()
	{
		if (m_isShown) 
		{
			m_isShown = false;
			m_cg.alpha = 0.0f;
			m_cg.blocksRaycasts = false;
			m_cg.interactable = false;
		} 
		else 
		{
			m_isShown = true;
			m_cg.alpha = 1.0f;
			m_cg.blocksRaycasts = true;
			m_cg.interactable = true;
		}
	}
}
