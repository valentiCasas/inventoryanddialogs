﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInfoScript : MonoBehaviour 
{
	public Text m_name;
	public Text m_description;
	public Image m_image;


	public void setItemName(string name)
	{
		m_name.text = name;
	}

	public void setItemDescription(string description)
	{
		m_description.text = description;
	}

	public void setItemImage(Sprite sprite)
	{
		m_image.sprite = sprite;
	}
}
