﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class IventoryItemScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Image m_itemImage;
	public Text m_amountText;

	private ItemData m_item;
	private GameObject m_itemInfo;

	void Start()
	{
		m_itemInfo = GameObject.Find ("ItemInfo");
	}

	void Update () 
	{
		updateAmountText ();
	}

	void updateAmountText()
	{
		m_amountText.text = "x" + m_item.m_amount;
	}

	public void addItem()
	{
		m_item.m_amount++;
	}

	public ItemData getItemData()
	{
		return m_item;
	}

	// Funcion que nos permite generar un objeto a partir de unos parametros
	public void loadItem(string name, string image, string description, int amount)
	{
		m_item = new ItemData (name, description, image, amount);

		Sprite[] sprites = Resources.LoadAll<Sprite> ("Smashbrositems");

		for (int i = 0; i < sprites.Length; i++)
		{
			if (sprites [i].name == m_item.m_image) 
			{
				m_itemImage.sprite = sprites[i];		
			}
		}
	}

	// Funcion que nos permite generar un objeto aleatorio de los prehechos
	public void loadRandomItem()
	{
		m_item = new ItemData ();

		Sprite[] sprites = Resources.LoadAll<Sprite> ("Smashbrositems");

		for (int i = 0; i < sprites.Length; i++)
		{
			if (sprites [i].name == m_item.m_image) 
			{
				m_itemImage.sprite = sprites[i];		
			}
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		CanvasGroup canvasGroup = m_itemInfo.GetComponent<CanvasGroup> ();
		canvasGroup.alpha = 1.0f;
		canvasGroup.blocksRaycasts = true;
		canvasGroup.interactable = true;

		ItemInfoScript infoScript = m_itemInfo.GetComponent<ItemInfoScript> ();
		infoScript.setItemName (m_item.m_name);
		infoScript.setItemDescription (m_item.m_description);
		infoScript.setItemImage (m_itemImage.sprite);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		CanvasGroup canvasGroup = m_itemInfo.GetComponent<CanvasGroup> ();
		canvasGroup.alpha = 0.0f;
		canvasGroup.blocksRaycasts = false;
		canvasGroup.interactable = false;
	}
}
