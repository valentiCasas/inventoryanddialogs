﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData
{
	public string m_name = ""; 			// Nombre del objeto
	public string m_description = ""; 	// Descripcion del objeto
	public string m_image = "";			// Nombre de la imagen del objeto
	public int m_amount = 0;			// Cantidad del objeto

	public ItemData()
	{
		int indice = Random.Range (0, SampleItems.samples.Count - 1);

		m_name = SampleItems.samples[indice].m_name;
		m_description = SampleItems.samples[indice].m_description;
		m_image = SampleItems.samples[indice].m_image;
		m_amount = SampleItems.samples[indice].m_amount;
	}

	public ItemData(string name, string description, string image, int amount)
	{
		m_name = name;
		m_description = description;
		m_image = image;
		m_amount = amount;
	}
}
