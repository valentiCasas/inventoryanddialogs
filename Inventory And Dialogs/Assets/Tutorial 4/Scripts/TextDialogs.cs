﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*	Representacion JSON de como guardar los dialogos

{
	"NombrePJ":{
		"aleatorios":["Dialogo1","Dialogo2","Dialogo3"], // Se coge 1 aleatorio de todos y se muestra
		"secuencia":["Dialogo1","Dialogo2","Dialogo3"]   // Se muestran 1 después de otro hasta el último (necesaria condición para que pase al siguiente)
	}
}

*/


public static class TextDialogs
{
	public static Dictionary<string,Dictionary<string, List<string>>> dialogs = new Dictionary<string, Dictionary<string, List<string>>>();

	public static void init()
	{
		Dictionary<string, List<string>> dialogTypes = new Dictionary<string, List<string>>();
		List<string> aleatoria = new List<string> () {"Gracias por mi caja !!"};
		List<string> secuencia = new List<string> () {"Buenas !! Necesito que recojas una caja de color amarillo", "Por favor busca mi caja..."};
		dialogTypes.Add (DialogType.RANDOM, aleatoria);
		dialogTypes.Add (DialogType.SEQUENCE, secuencia);
		dialogs.Add ("NPC_1", dialogTypes);

		dialogTypes = new Dictionary<string, List<string>>();
		aleatoria = new List<string> () {"¿No tienes otra cosa mejor que hacer?", "¡Vete por ahí!"};
		secuencia = new List<string> ();
		dialogTypes.Add (DialogType.RANDOM, aleatoria);
		dialogTypes.Add (DialogType.SEQUENCE, secuencia);
		dialogs.Add ("NPC_2", dialogTypes);
	}
}
