﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO; 
using UnityEngine.UI;
/*
[System.Serializable]
public class DialogsList 
{
	public List<DialogsData> dialogs = new List<DialogsData>();
}

[System.Serializable]
public class DialogsData 
{
	public string character;
	public List<string> dialogs = new List<string>();
}*/

public static class DialogType
{
	public const string RANDOM = "aleatorios";
	public const string SEQUENCE = "secuencia";
}

public class DialogController : Singleton<DialogController> {

	protected DialogController () {}

	void Awake () 
	{
		TextDialogs.init ();
	}

	static public T RegisterComponent<T> () where T: Component 
	{
		return Instance.GetOrAddComponent<T>();
	}

	public List<string> getDialogForChar(string character, string type)
	{
		List<string> dialogs;

		if (TextDialogs.dialogs.ContainsKey (character)) 
		{
			dialogs = TextDialogs.dialogs [character][type];
		} 
		else 
		{
			dialogs = null;
		}
		 
		return dialogs;
	}

}

