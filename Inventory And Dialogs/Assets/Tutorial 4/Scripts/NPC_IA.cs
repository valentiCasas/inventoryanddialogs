﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC_IA : MonoBehaviour {

	public string m_dialogType = DialogType.RANDOM;

	private DialogPanelController dialogController = null;
	private GameObject m_target = null;
	private Text m_infoText = null;

	private bool m_lookTarget = false;

	private List<string> m_dialogs;
	private int dialogIdx = 0;

	void Start () 
	{
		dialogController = GameObject.Find ("DialogPanel").GetComponent<DialogPanelController>();
		m_infoText = GameObject.Find ("InfoText").GetComponent<Text>();
		m_dialogs = DialogController.Instance.getDialogForChar (gameObject.name, m_dialogType);
	}

	void Update () 
	{
		if (m_lookTarget) 
		{
			transform.LookAt (m_target.transform);
			updateInfoText("Apreta Left Ctrl para hablar");

			if (Input.GetButtonDown ("Fire1")) 
			{
				updateInfoText("");
				displayDialog ();
			}
		} 
	}

	public void changeDialogType()
	{
		if (m_dialogType == DialogType.RANDOM) 
		{
			m_dialogType = DialogType.SEQUENCE;
		} 
		else 
		{
			m_dialogType = DialogType.RANDOM;
		}

		m_dialogs = DialogController.Instance.getDialogForChar (gameObject.name, m_dialogType);
	}

	void displayDialog()
	{
		if (m_dialogs != null) 
		{	
			if (m_dialogType == DialogType.RANDOM) 
			{
				dialogController.updateDialogText (m_dialogs [Random.Range (0, m_dialogs.Count)]);
			} 
			else if (m_dialogType == DialogType.SEQUENCE) 
			{
				dialogController.updateDialogText (m_dialogs [dialogIdx]);
				dialogIdx++;
				dialogIdx = Mathf.Min(dialogIdx, m_dialogs.Count - 1); 
			}
		} 
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			m_target = col.gameObject;
			m_lookTarget = true;
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			m_target = null;
			m_lookTarget = false;
		}
	}

	void updateInfoText(string text)
	{
		if (dialogController.getDialogText() != "") 
		{
			m_infoText.text = "";
		} 
		else 
		{
			m_infoText.text = text;
		}
	}

}
