﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogPanelController : MonoBehaviour {

	public Text m_dialogText;
	private float m_dissappear = 2.0f;
	private float m_accumulated = 0.0f;

	public void updateDialogText(string text)
	{
		m_dialogText.text = text;
		m_accumulated = 0.0f;
	}

	public string getDialogText()
	{
		return m_dialogText.text;
	}

	void Update () 
	{
		if (m_dialogText.text != "") 
		{
			CanvasGroup cg = GetComponent<CanvasGroup> ();
			cg.alpha = 1;
			cg.interactable = true;
			cg.blocksRaycasts = true;

			m_accumulated += Time.deltaTime;

			if (m_accumulated >= m_dissappear)
			{
				m_accumulated = 0.0f;
				m_dialogText.text = "";
			}
		} 
		else 
		{
			CanvasGroup cg = GetComponent<CanvasGroup> ();
			cg.alpha = 0;
			cg.interactable = false;
			cg.blocksRaycasts = false;
		}
	}
}
