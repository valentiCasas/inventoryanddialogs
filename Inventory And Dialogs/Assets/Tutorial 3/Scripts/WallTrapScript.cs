﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTrapScript : MonoBehaviour {

	private bool shouldMove = false;
	private float velocitat = 2.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (shouldMove) 
		{
			Vector3 pos = transform.position;
			pos.z -= velocitat * Time.deltaTime;
			transform.position = pos;
		}
		
	}

	public void startMovingWall()
	{
		shouldMove = true;
	}
}
