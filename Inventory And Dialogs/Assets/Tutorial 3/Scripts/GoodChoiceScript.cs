﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoodChoiceScript : MonoBehaviour {

	public Text display;
	public WallTrapScript wallTrap;

	bool activated = false;

	void Update()
	{
		if (activated && Input.GetKeyDown (KeyCode.E)) 
		{
			wallTrap.gameObject.SetActive (false);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			activated = true;
			display.text = "Press E to deactivate the wall trap";
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			activated = false;
			display.text = "";
		}
	}
}
