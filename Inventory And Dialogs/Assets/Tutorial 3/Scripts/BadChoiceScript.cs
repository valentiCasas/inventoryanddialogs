﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadChoiceScript : MonoBehaviour {

	public WallTrapScript wallTrap;
	public GameObject goodChoice;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			wallTrap.startMovingWall ();
			goodChoice.SetActive (true);
		}
	}
}
