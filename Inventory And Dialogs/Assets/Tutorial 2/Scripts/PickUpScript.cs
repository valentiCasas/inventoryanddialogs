﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpScript : MonoBehaviour {

	public InventoryControllerScript m_inventory;
	public NPC_IA npc;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{	
			m_inventory.addRandomItem ();

			if (npc != null)
				npc.changeDialogType ();

			Destroy (gameObject);
		}
	}

}
