﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCamera : MonoBehaviour {

	// Public
	public GameObject targetObject;

	// Private
	private Transform targetTrans; // Component needed to know where is the target
	private Renderer targetRender; // Component needed to know what size has the target

	private float distance = 3.0f; 	// Distance between the camera and the target
	private float height = 0.0f;	// Height from ground where the camera will be

	void Start () 
	{
		targetTrans = targetObject.GetComponent<Transform> ();
		targetRender = targetObject.GetComponentInChildren<Renderer> ();

		initThirdPersonCamera ();
	}

	void Update () 
	{
		followThirdPersonCamera ();
	}

	// Third Person Camera Functions //
	void initThirdPersonCamera()
	{
		// We add a little rotation to the camera
		transform.rotation = Quaternion.Euler (new Vector3 (15.0f, 0.0f, 0.0f));

		// The height to position the camera
		height = targetRender.bounds.size.y;
	}

	void followThirdPersonCamera()
	{
		// We retrieve the rotation of the target
		Vector3 targetRotation = targetTrans.rotation.eulerAngles;

		// We make some trigonometric calculations to set the distance of the target 
		float ConstX = Mathf.Sin (Mathf.Deg2Rad * targetRotation.y);
		float ConstZ = Mathf.Cos (Mathf.Deg2Rad * targetRotation.y);

		// We create the position of the camera from the target position and we add the constants
		Vector3 pos = new Vector3 (targetTrans.position.x - distance * ConstX, 
								   targetTrans.position.y + height, 
								   targetTrans.position.z - distance * ConstZ);
		transform.position = pos;

		// We keep the initial rotation of the X axis
		targetRotation.x = 15.0f;

		// We keep the same Z axis rotation
		targetRotation.z = transform.rotation.z;
		transform.rotation = Quaternion.Euler(targetRotation);
	}
}
