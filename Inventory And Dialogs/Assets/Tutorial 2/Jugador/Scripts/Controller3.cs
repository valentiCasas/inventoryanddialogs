﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller3 : MonoBehaviour {

	// Public
	public float force = 5.0f;
	public float rotateSpeed = 90.0f;

	// Private
	private Animator anim;
	private Rigidbody body;

	private float groundDistance = 0.1f;

	void Start () 
	{
		anim = GetComponent<Animator> ();
		body = GetComponent<Rigidbody> ();
	}

	void FixedUpdate ()
	{
		Direction ();
		Move ();
	}

	void Move()
	{
		if (IsGrounded() && Input.GetButtonDown ("Jump")) 
		{
			body.AddForce (Vector3.up * force, ForceMode.Impulse);
		}

		float speed = Input.GetAxis ("Vertical");
		anim.SetFloat ("Speed", speed);
	}

	void Direction()
	{
		float turn = Input.GetAxis ("Horizontal");
		transform.Rotate (Vector3.up, rotateSpeed * turn * Time.deltaTime);
	}

	bool IsGrounded()
	{
		return Physics.Raycast (transform.position, Vector3.down, groundDistance);
	}
}
